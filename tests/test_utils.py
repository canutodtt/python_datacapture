from unittest import TestCase

from src.utils import is_valid_number, is_valid_range
from tests.fixtures.mocks import NUMBERS_FOR_STATS


class TestUtils(TestCase):
    def test_valid_number(self):
        for number in NUMBERS_FOR_STATS:
            is_valid = is_valid_number(number)
            self.assertTrue(is_valid)

    def test_invalid_numbers(self):
        invalid_numbers = [-100, 0, "String", True, {}, []]
        for invalid_number in invalid_numbers:
            is_valid = is_valid_number(invalid_number)
            self.assertFalse(is_valid)

    def test_valid_range(self):
        valid_ranges = [(1, 2), (100, 200), (-100, 100), (500, 3000), (0, 5)]
        for range in valid_ranges:
            minor, major = range
            is_valid = is_valid_range(minor, major)
            self.assertTrue(is_valid)

    def test_invalid_range(self):
        invalid_ranges = [(2, 1), (200, 100), (100, -100), (3000, 500), (5, 0)]
        for range in invalid_ranges:
            minor, major = range
            is_valid = is_valid_range(minor, major)
            self.assertFalse(is_valid)
