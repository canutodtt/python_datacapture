from unittest import TestCase

from src.data_capture import DataCapture
from src.stats import Stats
from tests.fixtures.mocks import NUMBERS_FOR_STATS


class TestStats(TestCase):
    def setUp(self):
        self._list = NUMBERS_FOR_STATS.copy()

    def initialize_datacapture(self, numbers_list):
        data_capture = DataCapture()
        for number in numbers_list:
            data_capture.add(number)
        return data_capture

    def test_set_empty_stats(self):
        data_capture = self.initialize_datacapture([])
        stats = Stats(data_capture)
        self.assertEqual(stats._stats, {})
        self.assertEqual(stats._length, 0)
        self.assertEqual(stats._max, 0)
        self.assertEqual(stats._min, -1)

    def test_set_stats(self):
        data_capture = self.initialize_datacapture(self._list)
        stats = Stats(data_capture)
        self.assertEqual(len(stats._stats), len(self._list))
        self.assertEqual(stats._length, len(self._list))
        min = 0
        max = self._list[-1] - 1
        for stat in stats._stats.items():
            _, details = stat
            less, greater = details
            self.assertEqual(less, min)
            self.assertEqual(greater, max)
            min += 1
            max -= 1

    def test_set_repeated_stats(self):
        self._list[100] = 1
        self._list[200] = 1
        self._list[300] = 1
        self._list[400] = 1
        self._list[500] = 1
        data_capture = self.initialize_datacapture(self._list)
        stats = Stats(data_capture)
        self.assertEqual(stats._length, len(self._list))
        self.assertEqual(len(stats._stats), len(self._list))

    def test_valid_less_number(self):
        data_capture = self.initialize_datacapture(self._list)
        stats = Stats(data_capture)
        self.assertTrue(stats.valid_less_number(500))
        self.assertTrue(stats.valid_less_number(2))
        self.assertTrue(stats.valid_less_number(1000))

    def test_invalid_less_number(self):
        """
        Test that less numbers should be positive, integers
        and major or equals to the minor value in the stats
        """
        self._list = self._list[5:]
        data_capture = self.initialize_datacapture(self._list)
        stats = Stats(data_capture)
        self.assertFalse(stats.valid_less_number(2))
        self.assertFalse(stats.valid_less_number(0))
        self.assertFalse(stats.valid_less_number(-5))
        self.assertFalse(stats.valid_less_number("S"))

    def test_less(self):
        data_capture = self.initialize_datacapture(self._list)
        stats = Stats(data_capture)
        result = stats.less(500)
        self.assertEqual(result, 499)

        result = stats.less(3000)
        self.assertEqual(result, 1000)

        _list2 = self._list.copy()
        _list2[600] = 1
        _list2[700] = 1
        _list2[800] = 1
        _list2[900] = 1
        _list2[950] = 1
        data_capture = self.initialize_datacapture(_list2)
        stats2 = Stats(data_capture)
        result = stats2.less(500)
        self.assertEqual(result, 504)

        _list3 = self._list[4::5]
        data_capture = self.initialize_datacapture(_list3)
        stats3 = Stats(data_capture)
        result = stats3.less(500)
        self.assertEqual(result, 99)

    def test_less_invalid(self):
        self._list = self._list[4::5]
        data_capture = self.initialize_datacapture(self._list)
        stats = Stats(data_capture)
        result = stats.less(1)
        self.assertEqual(result, 0)

        result = stats.less(0)
        self.assertEqual(result, 0)

        result = stats.less(-5)
        self.assertEqual(result, 0)

        result = stats.less("String")
        self.assertEqual(result, 0)

    def test_valid_greater_number(self):
        data_capture = self.initialize_datacapture(self._list)
        stats = Stats(data_capture)
        self.assertTrue(stats.valid_greater_number(500))
        self.assertTrue(stats.valid_greater_number(999))
        self.assertTrue(stats.valid_greater_number(1))

    def test_invalid_greater_number(self):
        """
        Test that greater numbers should be positive, integers
        and minor or equals to the bigest value in the stats
        """
        self._list = self._list[:50]
        data_capture = self.initialize_datacapture(self._list)
        stats = Stats(data_capture)
        self.assertFalse(stats.valid_greater_number(52))
        self.assertFalse(stats.valid_greater_number(0))
        self.assertFalse(stats.valid_greater_number(-5))
        self.assertFalse(stats.valid_greater_number("S"))

    def test_greater(self):
        data_capture = self.initialize_datacapture(self._list)
        stats = Stats(data_capture)
        result = stats.greater(500)
        self.assertEqual(result, 500)

        _list2 = self._list.copy()
        _list2[0] = 600
        _list2[100] = 600
        _list2[200] = 600
        _list2[300] = 600
        _list2[400] = 600
        data_capture = self.initialize_datacapture(_list2)
        stats2 = Stats(data_capture)
        result = stats2.greater(500)
        self.assertEqual(result, 505)

        _list3 = self._list[4::5]
        data_capture = self.initialize_datacapture(_list3)
        stats3 = Stats(data_capture)
        result = stats3.greater(500)
        self.assertEqual(result, 100)

        result = stats3.greater(1)
        self.assertEqual(result, 200)

    def test_greater_invalid(self):
        self._list = self._list[4::5]
        data_capture = self.initialize_datacapture(self._list)
        stats = Stats(data_capture)
        result = stats.greater(1000)
        self.assertEqual(result, 0)

    def test_between(self):
        data_capture = self.initialize_datacapture(self._list)
        stats = Stats(data_capture)
        result = stats.between(1, 500)
        self.assertEqual(result, 500)

        _list2 = self._list.copy()
        _list2[500] = 6
        _list2[600] = 6
        _list2[700] = 6
        _list2[800] = 6
        _list2[900] = 6
        data_capture = self.initialize_datacapture(_list2)
        stats2 = Stats(data_capture)
        result = stats2.between(1, 400)
        self.assertEqual(result, 405)

        _list3 = self._list.copy()
        _list3[0] = 600
        _list3[100] = 600
        _list3[200] = 600
        _list3[300] = 600
        _list3[400] = 600
        data_capture = self.initialize_datacapture(_list3)
        stats3 = Stats(data_capture)
        result = stats3.between(501, 800)
        self.assertEqual(result, 305)

        _list4 = self._list[4::5]
        data_capture = self.initialize_datacapture(_list4)
        stats4 = Stats(data_capture)
        result = stats4.between(501, 900)
        self.assertEqual(result, 80)
