from unittest import TestCase
from unittest.mock import patch

from src.data_capture import DataCapture
from src.stats import Stats
from tests.fixtures.mocks import NUMBERS_FOR_STATS


class TestDataCapture(TestCase):
    def setUp(self):
        self.data_capture = DataCapture()

    def add_numbers(self, numbers):
        for number in numbers:
            self.data_capture.add(number)

    def test_add_numbers(self):
        self.add_numbers(NUMBERS_FOR_STATS)
        self.assertEqual(self.data_capture._length, 1000)
        self.assertEqual(self.data_capture._min, 1)
        self.assertEqual(self.data_capture._max, 1000)

    @patch("src.data_capture.logger.error")
    def test_add_negative_number(self, error_log):
        stats = NUMBERS_FOR_STATS.copy()
        stats[100] = -10
        stats[200] = -20
        stats[300] = -30
        stats[400] = -40
        stats[500] = -50
        self.add_numbers(stats)
        self.assertEqual(self.data_capture._length, 995)
        self.assertEqual(self.data_capture._min, 1)
        self.assertEqual(self.data_capture._max, 1000)
        error_log.assert_called()

    @patch("src.data_capture.logger.error")
    def test_add_no_integer_value(self, error_log):
        stats = NUMBERS_FOR_STATS.copy()
        stats[100] = "100"
        stats[200] = "200"
        stats[300] = "300"
        stats[400] = "400"
        stats[500] = "500"
        self.add_numbers(stats)
        self.assertEqual(self.data_capture._length, 995)
        self.assertEqual(self.data_capture._min, 1)
        self.assertEqual(self.data_capture._max, 1000)
        error_log.assert_called()

    @patch("src.data_capture.logger.error")
    def test_combined_values(self, error_log):
        stats = NUMBERS_FOR_STATS.copy()
        stats[100] = "100"
        stats[200] = -200
        stats[300] = "3OO"
        stats[400] = -400
        stats[500] = "S00"
        self.add_numbers(stats)
        self.assertEqual(self.data_capture._length, 995)
        self.assertEqual(self.data_capture._min, 1)
        self.assertEqual(self.data_capture._max, 1000)
        error_log.assert_called()

    def test_build_empty_stats(self):
        stats = self.data_capture.build_stats()
        self.assertIsInstance(stats, Stats)

    def test_build_normal_stats(self):
        self.add_numbers(NUMBERS_FOR_STATS)
        stats = self.data_capture.build_stats()
        self.assertIsInstance(stats, Stats)

    def test_build_stats_with_negative_numbers(self):
        stats = NUMBERS_FOR_STATS.copy()
        stats[100] = -100
        stats[200] = -200
        stats[300] = -300
        stats[400] = -400
        stats[500] = -500
        self.add_numbers(stats)
        stats = self.data_capture.build_stats()
        self.assertIsInstance(stats, Stats)

    def test_build_stats_with_no_numbers(self):
        stats = NUMBERS_FOR_STATS.copy()
        stats[100] = "100"
        stats[200] = "200"
        stats[300] = "300"
        stats[400] = "400"
        stats[500] = "500"
        self.add_numbers(stats)
        stats = self.data_capture.build_stats()
        self.assertIsInstance(stats, Stats)

    def test_build_stats_with_combined_type_numbers(self):
        stats = NUMBERS_FOR_STATS.copy()
        stats[100] = "100"
        stats[200] = -200
        stats[300] = "300"
        stats[400] = -400
        stats[500] = "500"
        self.add_numbers(stats)
        stats = self.data_capture.build_stats()
        self.assertIsInstance(stats, Stats)
