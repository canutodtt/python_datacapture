import logging
from collections import defaultdict
from typing import TYPE_CHECKING

from src.utils import is_valid_number, is_valid_range

if TYPE_CHECKING:
    from src.data_capture import DataCapture
logger = logging.getLogger(__name__)


class Stats(object):
    """
    Class that store an amount of positive integers
    and returns statistics
    """

    def __init__(self, data_capture: "DataCapture") -> None:
        logger.debug("Starting Stats")
        self._min: int = data_capture._min
        self._max: int = data_capture._max
        self._length: int = data_capture._length
        self._stats: dict = defaultdict(lambda: (self._length, self._length))
        if self._length:
            self.set_stats(data_capture._numbers_dict)

    def set_stats(self, numbers_dict: dict) -> dict:
        """
        Creates a dictionary based on the min and max numbers captures
        each number key will consist in a set of the amount numbers
        lower and bigger inside the list

        :param list numbers_dict: dict of numbers with the number of ocurrences
        :return: dict formatted as
            {
                number: (
                    amount of numbers lower than it,
                    amount of numbers bigger than it
                )
            }
        """
        greater: int = self._length
        less: int = 0
        for number in range(self._min, self._max + 1):
            ocurrence: int = numbers_dict[number]
            greater -= ocurrence
            self._stats[number] = (less, greater)
            less += ocurrence
        return self._stats

    def valid_less_number(self, number: int) -> bool:
        """
        Verify that number given is not less than the lowest added in the stats

        :param int number: number to verify if is integer,
            positive and not the lowest
        :return: boolean if the number is valid
        """
        return is_valid_number(number) and number >= self._min

    def less(self, number: int) -> int:
        """
        Gets the amount of numbers on the stats lower than
        the number given as paramater

        :param int number: initial number to get the lower stats
        """
        result: int = 0
        if self._length and self.valid_less_number(number):
            result, _ = self._stats[number]
        return result

    def valid_greater_number(self, number: int) -> bool:
        """
        Verify that number given is not greater than the biggest
        added in the stats

        :param int number: number to verify if is integer,
            positive and not the bigest one
        :return: boolean if the number is valid
        """
        return is_valid_number(number) and number <= self._max

    def greater(self, number: int) -> int:
        """
        Gets the amount of numbers on the stats bigger than
        the number given as paramater

        :param int number: initial number to get the bigger stats
        """
        result: int = 0
        if self._length and self.valid_greater_number(number):
            _, result = self._stats[number]
        return result

    def between(self, minor: int, major: int) -> int:
        """
        Gets the amount of numbers on the stats between
        a range given as parameters

        :param int minor: starting number of the range
        :param int major: ending number of the range
        :return: int number of the stats between the range given
        """
        result: int = 0
        valid_minor: bool = self.valid_greater_number(minor)
        valid_major: bool = self.valid_less_number(major)
        if (
            self._length
            and valid_minor
            and valid_major
            and is_valid_range(minor, major)
        ):
            min: int = self.less(minor)
            max: int = self.greater(major)
            result = self._length - (min + max)
        return result
