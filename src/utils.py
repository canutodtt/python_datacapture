import logging

logger = logging.getLogger(__name__)


def is_valid_number(number: int) -> bool:
    """
    Verifies that number is posiive and integer

    :param int number: number to be evaluated
    :return: boolean value
    """
    is_integer: bool = type(number) == int
    is_positive: bool = False
    try:
        is_positive = number > 0
    except TypeError:
        message: str = "{} can't be validated as positive number"
        logger.warning(message.format(number))
    if not is_integer:
        logger.error("{} is not consider as integer".format(number))
    if not is_positive:
        logger.error("{} is not a positive number".format(number))
    return is_integer and is_positive


def is_valid_range(minor: int, major: int) -> bool:
    """
    Verifies that exists a range between the minor and major values

    :param int minor: start number in range
    :param int major: stop number in range
    "return: boolean value
    """
    valid_range: bool = minor < major
    if not valid_range:
        message: str = "Value {} should be minor than {} to stablish a range"
        logger.error(message.format(minor, major))
    return valid_range
