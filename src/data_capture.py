import logging
from collections import defaultdict

from src.stats import Stats
from src.utils import is_valid_number

logger = logging.getLogger(__name__)


class DataCapture(object):
    """Class that will capture an N amount of positive integers in a list"""

    def __init__(self) -> None:
        logger.debug("Initializating DataCapture class")
        self._numbers_dict: dict = defaultdict(int)
        self._length: int = 0
        self._max: int = 0
        self._min: int = -1

    def set_max_value(self, number: int) -> int:
        return number if number > self._max else self._max

    def set_min_value(self, number: int) -> int:
        return number if self._min == -1 or number < self._min else self._min

    def add(self, number: int) -> None:
        """
        Function to add a positive integer into the list of numbers

        :param int number: positive number to be added in the list
        """
        if is_valid_number(number):
            logger.debug("Adding number {} into the list".format(number))
            self._length += 1
            self._numbers_dict[number] += 1
            self._max = self.set_max_value(number)
            self._min = self.set_min_value(number)
        else:
            message: str = (
                "Error adding value {} into the list,"
                "this should be integer and positive"
            )
            logger.error(message.format(number))

    def build_stats(self) -> Stats:
        """
        Function that creates and returns an Stats class

        :return: class Stats
        """
        return Stats(self)
