import logging

from src.data_capture import DataCapture

log_format = "%(asctime)s %(levelname)s: %(message)s"
log_date_format = "%Y-%m-%d %H:%M:%S"
level = logging.NOTSET
logging.basicConfig(format=log_format, datefmt=log_date_format, level=level)

capture = DataCapture()
capture.add(3)
capture.add(9)
capture.add(3)
capture.add(4)
capture.add(6)
stats = capture.build_stats()
print(stats.less(4))
print(stats.greater(4))
print(stats.between(3, 6))
